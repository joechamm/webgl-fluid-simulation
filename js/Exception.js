function ContextException (msg) {
	this.msg = msg;
	this.name = "ContextException";
	this.toString = function () {
		return this.name + ":" + this.msg;
	};
}	

function FramebufferException (msg) {
	this.msg = msg;
	this.name = "FramebufferException";
	this.toString = function () {
		return this.name + ":" + this.msg;
	};
}

function TextureException (msg) {
	this.msg = msg;
	this.name = "TextureException";
	this.toString = function () {
		return this.name + ":" + this.msg;
	};
}

function ShaderException (msg) {
	this.msg = msg;
	this.name = "ShaderException";
	this.toString = function () {
		return this.name + ":" + this.msg;
	};
}

function ProgramException (msg) {
	this.msg = msg;
	this.name = "ProgramException";
	this.toString = function () {
		return this.name + ":" + this.msg;
	};
}
