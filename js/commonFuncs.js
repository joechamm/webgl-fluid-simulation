
function displayArrayBufferInfo(gl, buff, shadProg, attribsToCheck)
{
    var str = "Array Buffer Info.<br/>";

    if(gl.isBuffer(buff))
    {
        str += "Buffer being checked is a valid WebGL buffer.<br/>";
    }
    else
    {
        str += "Buffer being checked is NOT a valid WebGL buffer.<br/>";
    }

    var numAttribsToCheck = attribsToCheck.length;

    gl.bindBuffer(gl.ARRAY_BUFFER, buff);

    str += "Size: ";
    str += gl.getBufferParameter(gl.ARRAY_BUFFER, gl.BUFFER_SIZE);
    str += "<br/>";

    for(var i = 0; i < numAttribsToCheck; i++)
    {
        var check = attribsToCheck[i];

        var activeAttribInfo = gl.getActiveAttrib(shadProg, check);
        str += "Attribute Active at index ";
        str += check;
        str += ": ";
        str += activeAttribInfo.name;
        str += "<br/>";
        str += "Size: ";
        str += activeAttribInfo.size;
        str += "<br/>";
        if(activeAttribInfo.type == gl.FLOAT)
        {
            str += "Type: Float";
            str += "<br/>";
        }
        else if(activeAttribInfo.type == gl.UNSIGNED_INT)
        {
            str += "Type: Unsigned Int";
            str += "<br/>";
        }
        else if(activeAttribInfo.type == gl.INT)
        {
            str += "Type: Int";
            str += "<br/>";
        }
        else
        {
            str += "Type: Unknown";
            str += "<br/>";
        }

        var vertAttribOffset = gl.getVertexAttribOffset(check, gl.VERTEX_ATTRIB_ARRAY_POINTER);

        str += "Vertex Attrib Offset for index ";
        str += check;
        str += ": ";
        str += vertAttribOffset;
        str += "<br/>";

        if(gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_ENABLED))
        {
            str += "Vertex Attrib Array at index ";
            str += check;
            str += " is Enabled";
            str += "<br/>";
        }
        else
        {
            str += "Vertex Attrib Array at index ";
            str += check;
            str += " is NOT Enabled";
            str += "<br/>";
        }

        str += "Vertex Attrib Array Size for index ";
        str += check;
        str += ": ";
        str += gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_SIZE);
        str += "<br/>";

        str += "Vertex Attrib Array Stride for index ";
        str += check;
        str += ": ";
        str += gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_STRIDE);
        str += "<br/>";

        var vAttType = gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_TYPE);

        if(vAttType == gl.FLOAT)
        {
            str += "Vertex Attrib Array Type: Float";
            str += "<br/>";
        }
        else if(vAttType == gl.UNSIGNED_INT)
        {
            str += "Vertex Attrib Array Type: Unsigned Int";
            str += "<br/>";
        }
        else if(vAttType == gl.INT)
        {
            str += "Vertex Attrib Array Type: Int";
            str += "<br/>";
        }
        else
        {
            str += "Vertex Attrib Array Type: Unknown";
            str += "<br/>";
        }
    }
    return str;
}

function dispWebGLInfo(gl)
{
    var str = "Displaying Current WebGL Info: <br/>";

    var maxVertAttribs = gl.getParameter(gl.MAX_VERTEX_ATTRIBS);
    var currentRenderer = gl.getParameter(gl.RENDERER);
    var shadingLangVers = gl.getParameter(gl.SHADING_LANGUAGE_VERSION);
    var currentVendor = gl.getParameter(gl.VENDOR);
    var currentVersion = gl.getParameter(gl.VERSION);

    str += "Max Vertex Attribs: ";
    str += maxVertAttribs;
    str += "<br/> Current Renderer: ";
    str += currentRenderer;
    str += "<br/> Shading Language Version: ";
    str += shadingLangVers;
    str += "<br/> Current Vendor: ";
    str += currentVendor;
    str += "<br/> Current Version: ";
    str += currentVersion;
    str += "<br/>";

    var wglContextAttribs = gl.getContextAttributes();

    str += "WebGLContext Attributes.<br/> Alpha: ";
    str += wglContextAttribs.alpha;
    str += "<br/>";
    str += "Depth: ";
    str += wglContextAttribs.depth;
    str += "<br/>";
    str += "Stencil: ";
    str += wglContextAttribs.stencil;
    str += "<br/>";
    str += "Antialias: ";
    str += wglContextAttribs.antialias;
    str += "<br/>";
    str += "PremultipliedAlpha: ";
    str += wglContextAttribs.premultipliedAlpha;
    str += "<br/>";
    str += "PreserveDrawingBuffer: ";
    str += wglContextAttribs.preserveDrawingBuffer;
    str += "<br/>";

    str += "Supported Extensions:<br/>";

    var supportedExtensions = gl.getSupportedExtensions();

    for(var i = 0; i < supportedExtensions.length; i++)
    {
        str += supportedExtensions[i];
        str += "<br/>";
    }

    return str;
}

function checkShaders(gl, whichShader)
{
    var str = "Displaying Current Shader Info: <br/>";

    str += "Program Info Log: <br/>";

    str += gl.getProgramInfoLog(whichShader);
    str += "<br/>";

    var attachedShaders = gl.getAttachedShaders(whichShader);

    var numShaders = attachedShaders.length;
    str += "Num Shaders: ";
    str += numShaders;
    str += "<br/>";

    for(var i = 0; i < numShaders; i++)
    {
        var tempShader = attachedShaders[i];

        if(gl.VERTEX_SHADER == gl.getShaderParameter(tempShader, gl.SHADER_TYPE))
        {
            str += "Type: Vertex Shader<br/>";
        }
        else
        {
            str += "Type: Fragment Shader<br/>";
        }

        var InfoLog = gl.getShaderInfoLog(tempShader);

        str += "InfoLog: <br/>";

        str += InfoLog;
        str += "<br/>";
    }
    return str;
}

function checkAttribs(gl, whichShader, whichBuffer, whichAttribs)
{
    gl.useProgram(whichShader);
    gl.bindBuffer(gl.ARRAY_BUFFER, whichBuffer);

    var str = "Displaying Current Attrib Info.<br/>";

    if(gl.isBuffer(whichBuffer))
    {
        str += "Buffer being checked is valid WebGL buffer.<br/>";
    }
    else
    {
        str += "Buffer being checked is NOT valid WebGL buffer.<br/>";
    }

    var numToCheck = whichAttribs.length;

    for(var i = 0; i < numToCheck; i++)
    {
        var check = whichAttribs[i];

        var activeAttInfo = gl.getActiveAttrib(whichShader, check);

        var IsEnabled = gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_ENABLED);

        var attribArraySize = gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_SIZE);
        var attribArrayStride = gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_STRIDE);
        var attribArrayType = gl.getVertexAttrib(check, gl.VERTEX_ATTRIB_ARRAY_TYPE);
        var attribArray = gl.getVertexAttrib(check, gl.CURRENT_VERTEX_ATTRIB);

        str += "Checking Attrib at index ";
        str += check;
        str += "<br/>";
        str += "Name: ";
        str += activeAttInfo.name;
        str += "<br/>";
        if(IsEnabled)
        {
            str += "Is Enabled<br/>";
        }
        else
        {
            str += "Not Enabled<br/>";
        }
        str += "Active Attrib Array Size: ";
        str += activeAttInfo.size;
        str += "<br/>";
        str += "Active Attrib Array Type: ";
        str += activeAttInfo.type;
        str += "<br/>";
        str += "Vertex Array Size: ";
        str += attribArraySize;
        str += "<br/>";
        str += "Vertex Array Stride: ";
        str += attribArrayStride;
        str += "<br/>";
        str += "Vertex Array Type: ";
        str += attribArrayType;
        str += "<br/>";
        str += "Vertex Array Values: (";
        str += attribArray[0];
        str += ", ";
        str += attribArray[1];
        str += ", ";
        str += attribArray[2];
        str += ", ";
        str += attribArray[3];
        str += ")";
        str += "<br/>";
    }

    return str;
}

function displayMatrixInfo(mat)
{
    // Lock the number of digits displayed to 5 to avoid oddly spaced text

    var str = "[ ";
    str += mat[0].toPrecision(5);
    str += " ";
    str += mat[4].toPrecision(5);
    str += " ";
    str += mat[8].toPrecision(5);
    str += " ";
    str += mat[12].toPrecision(5);
    str += "]<br/>";

    str += "[ ";
    str += mat[1].toPrecision(5);
    str += " ";
    str += mat[5].toPrecision(5);
    str += " ";
    str += mat[9].toPrecision(5);
    str += " ";
    str += mat[13].toPrecision(5);
    str += "]<br/>";

    str += "[ ";
    str += mat[2].toPrecision(5);
    str += " ";
    str += mat[6].toPrecision(5);
    str += " ";
    str += mat[10].toPrecision(5);
    str += " ";
    str += mat[14].toPrecision(5);
    str += "]<br/>";

    str += "[ ";
    str += mat[3].toPrecision(5);
    str += " ";
    str += mat[7].toPrecision(5);
    str += " ";
    str += mat[11].toPrecision(5);
    str += " ";
    str += mat[15].toPrecision(5);
    str += "]<br/>";

    return str;
}

function vectorAddition(vec0, vec1){
    var retVec = new Array();
    retVec[0] = vec1[0] + vec0[0];
    retVec[1] = vec1[1] + vec0[1];
    retVec[2] = vec1[2] + vec0[2];

    return retVec;
}

function vectorDifference(vec0, vec1){
    var retVec = new Array();
    retVec[0] = vec1[0] - vec0[0];
    retVec[1] = vec1[1] - vec0[1];
    retVec[2] = vec1[2] - vec0[2];
    return retVec;
}

function normalizeVec(vector){
    var len = Math.sqrt(((vector[0] * vector[0]) + (vector[1] * vector[1]) + (vector[2] * vector[2])));
    var retVec = new Array();
    retVec[0] = vector[0] / len;
    retVec[1] = vector[1] / len;
    retVec[2] = vector[2] / len;
    return retVec;
}

function crossProduct(vec0, vec1){
    var cross = new Array();

    cross[0] = vec0[1] * vec1[2] - vec0[2] * vec1[1];
    cross[1] = vec0[2] * vec1[0] - vec0[0] * vec1[2];
    cross[2] = vec0[0] * vec1[1] - vec0[1] * vec1[0];

    return cross;
}

function calcTriangleNormal(point0, point1, point2){
    var d1 = vectorDifference(point0, point1);
    var d2 = vectorDifference(point0, point2);

    var tempNormal = crossProduct(d1, d2);

    return normalizeVec(tempNormal);
}

function degToRadians(degrees){
    return (degrees * Math.PI) / 180.0;
}

function radToDegrees(rads){
    return (180.0 * rads) / (Math.PI);
}

function Point2D(xCoord, yCoord){
    this.xCoord = xCoord;
    this.yCoord = yCoord;

    this.distance2D = distance2D;
    this.transPoint = transPoint;
}

function distance2D(otherPoint)
{
    var xDiff = otherPoint.xCoord - this.xCoord;
    var yDiff = otherPoint.yCoord - this.yCoord;

    return Math.sqrt((xDiff * xDiff) + (yDiff * yDiff));
}

function transPoint(xTrans, yTrans){
    return new Point2D((this.xCoord + xTrans), (this.yCoord + yTrans));
}

function Quad(centerPoint, halfLength, parent, quadId)  // quadId determined by the formula: (parent.quadId * 4) + location id (i.e. 1 = ne, 2 = nw, 3 = sw, 4 = se) with 0 being reserved for root
{
    this.centerPoint = centerPoint;
    this.halfLength = halfLength;
    this.parent = parent;
    this.depthLevel = parent.depthLevel + 1;
    this.neChild = null;
    this.nwChild = null;
    this.swChild = null;
    this.seChild = null;
    this.quadId = quadId;
    this.interiorPoints = [];

    if(parent)
    {
        this.maxDepth = parent.maxDepth;
    }
    else
    {
        this.maxDepth = 8;
    }

    this.setMaxDepth = setMaxDepth;
    this.insertPoint = insertPoint;
    this.determineSubQuad = determineSubQuad;
    this.isInterior = isInterior;
    this.getRect = getRect;
    this.getCross = getCross;
    this.isEmpty = isEmpty;
    this.deleteChildren = deleteChildren;
    this.getInfo = getInfo;
}

function getInfo()
{
    var st = "Info for Quad: ";
    st += this.quadId;
    st += "<br/>";

    st += "Center: (";
    st += this.centerPoint.xCoord;
    st += ", ";
    st += this.centerPoint.yCoord;
    st += ")<br/>";

    st += "Half Length: ";
    st += this.halfLength;
    st += "<br/>";

    st += "Depth: ";
    st += this.depthLevel;
    st += "<br/>";

    st += "Number of interior points: ";
    st += this.interiorPoints.length;
    st += "<br/>";

    st += "Children: ";
    if(this.neChild)
    {
        st += "1, ";
    }
    if(this.nwChild)
    {
        st += "2, ";
    }
    if(this.swChild)
    {
        st += "3, ";
    }
    if(this.seChild)
    {
        st += "4";
    }
    st += "<br/>";

    return st;
}

function deleteChildren()
{
    if(this.neChild.isEmpty())
    {
        delete this.neChild;
    }
    else
    {
        this.neChild.deleteChildren();
    }

    if(this.nwChild.isEmpty())
    {
        delete this.nwChild;
    }
    else
    {
        this.nwChild.deleteChildren();
    }

    if(this.swChild.isEmpty())
    {
        delete this.swChild;
    }
    else
    {
        this.swChild.deleteChildren();
    }

    if(this.seChild.isEmpty())
    {
        delete this.seChild;
    }
    else
    {
        this.seChild.deleteChildren();
    }

    delete this.interiorPoints;
}

function isEmpty()
{
    return (this.interiorPoints.length == 0);
}

function setMaxDepth(newMaxDepth){
    this.maxDepth = newMaxDepth;
}

function isInterior(pointToCheck)
{
    var retValue = false;
    if(pointToCheck.xCoord < (this.centerPoint.xCoord + this.halfLength))
    {
        if(pointToCheck.xCoord > (this.centerPoint.xCoord - this.halfLength))
        {
            if(pointToCheck.yCoord < (this.centerPoint.yCoord + this.halfLength))
            {
                if(pointToCheck.yCoord > (this.centerPoint.yCoord - this.halfLength))
                {
                    retValue = true;
                }
            }
        }
    }
    return retValue;
}

function determineSubQuad(pointToCheck)
{
    if(pointToCheck.xCoord > this.centerPoint.xCoord)
    {
        if(pointToCheck.yCoord > this.centerPoint.yCoord)
        {
            return 0;
        }
        else
        {
            return 3;
        }
    }
    else
    {
        if(pointToCheck.yCoord > this.centerPoint.yCoord)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
}

function insertPoint(iPoint){
    var subToInsert = this.determineSubQuad(iPoint);

    if(subToInsert == 0)
    {
        if(this.neChild)
        {
            this.neChild.insertPoint(iPoint);  // there must already be a resident point as the child already exists.
        }
        else
        {
            if(this.interiorPoints.length > 0)  // there is already an interior point, and we need to create the subQuad
            {
                // if we are still below this max depth threshold, we create the new subquad, push the point into this quad's
                // list of interior points, then finally insert the point into the newly created sub quad.
                if(this.depthLevel < this.maxDepth)
                {
                    var neCenterPoint = new Point2D((this.centerPoint.xCoord + this.halfLength), (this.centerPoint.yCoord + this.halfLength));

                    this.neChild = new Quad(neCenterPoint, (this.halfLength * 0.5), this, (this.quadId * 4 + 1));

                    this.interiorPoints.push(iPoint);

                    this.neChild.insertPoint(iPoint);
                }
                else  // we've reached the maximum depth and can go no deeper
                {
                    this.interiorPoints.push(iPoint);
                }
            }
            else  // in this case we are in an empty quad and no children need to be created yet. Just add the point to this quad's list;
            {
                this.interiorPoints.push(iPoint);
            }
        }
    }
    else if(subToInsert == 1)
    {
        if(this.nwChild)
        {
            this.nwChild.insertPoint(iPoint);  // there must already be a resident point as the child already exists.
        }
        else
        {
            if(this.interiorPoints.length > 0)  // there is already an interior point, and we need to create the subQuad
            {
                // if we are still below this max depth threshold, we create the new subquad, push the point into this quad's
                // list of interior points, then finally insert the point into the newly created sub quad.
                if(this.depthLevel < this.maxDepth)
                {
                    var nwCenterPoint = new Point2D((this.centerPoint.xCoord - this.halfLength), (this.centerPoint.yCoord + this.halfLength));

                    this.nwChild = new Quad(nwCenterPoint, (this.halfLength * 0.5), this, (this.quadId * 4 + 2));

                    this.interiorPoints.push(iPoint);

                    this.nwChild.insertPoint(iPoint);
                }
                else  // we've reached the maximum depth and can go no deeper
                {
                    this.interiorPoints.push(iPoint);
                }
            }
            else  // in this case we are in an empty quad and no children need to be created yet. Just add the point to this quad's list;
            {
                this.interiorPoints.push(iPoint);
            }
        }
    }
    else if(subToInsert == 2)
    {
        if(this.swChild)
        {
            this.swChild.insertPoint(iPoint);  // there must already be a resident point as the child already exists.
        }
        else
        {
            if(this.interiorPoints.length > 0)  // there is already an interior point, and we need to create the subQuad
            {
                // if we are still below this max depth threshold, we create the new subquad, push the point into this quad's
                // list of interior points, then finally insert the point into the newly created sub quad.
                if(this.depthLevel < this.maxDepth)
                {
                    var swCenterPoint = new Point2D((this.centerPoint.xCoord - this.halfLength), (this.centerPoint.yCoord - this.halfLength));

                    this.swChild = new Quad(swCenterPoint, (this.halfLength * 0.5), this, (this.quadId * 4 + 3));

                    this.interiorPoints.push(iPoint);

                    this.swChild.insertPoint(iPoint);
                }
                else  // we've reached the maximum depth and can go no deeper
                {
                    this.interiorPoints.push(iPoint);
                }
            }
            else  // in this case we are in an empty quad and no children need to be created yet. Just add the point to this quad's list;
            {
                this.interiorPoints.push(iPoint);
            }
        }
    }
    else
    {
        if(this.seChild)
        {
            this.seChild.insertPoint(iPoint);  // there must already be a resident point as the child already exists.
        }
        else
        {
            if(this.interiorPoints.length > 0)  // there is already an interior point, and we need to create the subQuad
            {
                // if we are still below this max depth threshold, we create the new subquad, push the point into this quad's
                // list of interior points, then finally insert the point into the newly created sub quad.
                if(this.depthLevel < this.maxDepth)
                {
                    var seCenterPoint = new Point2D((this.centerPoint.xCoord + this.halfLength), (this.centerPoint.yCoord - this.halfLength));

                    this.seChild = new Quad(seCenterPoint, (this.halfLength * 0.5), this, (this.quadId * 4 + 4));

                    this.interiorPoints.push(iPoint);

                    this.seChild.insertPoint(iPoint);
                }
                else  // we've reached the maximum depth and can go no deeper
                {
                    this.interiorPoints.push(iPoint);
                }
            }
            else  // in this case we are in an empty quad and no children need to be created yet. Just add the point to this quad's list;
            {
                this.interiorPoints.push(iPoint);
            }
        }
    }
}

function getRect()
{
    var rectList = new Array();
    rectList.push(this.centerPoint.transPoint(this.halfLength, this.halfLength)); // ne corner
    rectList.push(this.centerPoint.transPoint(this.halfLength, -this.halfLength)); // nw corner
    rectList.push(this.centerPoint.transPoint(-this.halfLength, -this.halfLength)); // sw corner
    rectList.push(this.centerPoint.transPoint(this.halfLength, -this.halfLength)); // se corner

    return rectList;
}

function getCross()
{
    var crossList = new Array();
    crossList.push(this.centerPoint.transPoint(this.halfLength, 0.0)); // right
    crossList.push(this.centerPoint.transPoint(0.0, this.halfLength)); // up
    crossList.push(this.centerPoint.transPoint(-this.halfLength, 0.0)); // left
    crossList.push(this.centerPoint.transPoint(0.0, -this.halfLength)); // down

    return crossList;
}

function quadTree(centerPoint, halfLength, maxDepth, pointList)
{
    this.rootNode = new Quad(centerPoint, halfLength, null, 0);
    this.rootNode.setMaxDepth(maxDepth);
    this.pointList = pointList;

    this.buildTree = buildTree;
    this.destroyTree = destroyTree;
}

function buildTree()
{
    for(var i = 0; i < this.pointList.length; i++)
    {
        this.rootNode.insertPoint(this.pointList[i]);
    }
}

function destroyTree()
{
    this.rootNode.deleteChildren();
}