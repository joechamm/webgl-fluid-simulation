

var SourceList = function(max_sources) {
	this.max_sources = max_sources;
	this.sources = [];
};

SourceList.prototype = {
	
	constructor: SourceList,

	toString: function() {
		var str = "[SourceList ";
		for (var i = 0, len = this.sources.length; i < len; i++) {
			str += this.sources[i] + " ";
		}
		str += "]";
		return str;
	},

	getSourceCount: function() {
		return this.sources.length;
	},

	pushSource: function(src) {
		try {
			if (this.sources.length < this.max_sources) {
				this.sources.push(src);
			} else {
				throw "Max sources reached";
			}
		} catch(e) {
			console.log(e);
		}
	},
	
	popSource: function() {
		try {
			if (this.sources.length > 0) {
				return this.sources.pop();
			} else {
				throw "Source List is empty";
			}
		} catch(e) {
			console.log(e);
		}
	},

	getSrcPos: function(idx) {
		
		try {
			if (idx < this.sources.length) {
				return this.sources[idx].position;
			} else {
				throw "Index out of range";
			}
		} catch(e) {
			console.log(e);
		}
	},

	getSrcVel: function(idx) {
	
		try {
			if (idx < this.sources.length) {
				return this.sources[idx].velocity;
			} else {
				throw "Index out of range!";
			}
		} catch(e) {
			console.log(e);
		}
	},

	getSrcTemp: function(idx) {
	
		try {
			if (idx < this.sources.length) {
				return this.sources[idx].temperature;
			} else {
				throw "Index out of range";
			}
		} catch(e) {
			console.log(e);
		}
	},

	getSrcRadius: function(idx) {
	
		try {
			if (idx < this.sources.length) {
				return this.sources[idx].radius;
			} else {
				throw "Index out of range";
			}
		} catch(e) {
			console.log(e);
		}
	},

	setSrcPos: function(idx, position) {	
		try {
			if (idx < this.sources.length) {
				this.sources[idx].position.x = position.x;
				this.sources[idx].position.y = position.y;
			} else {
				throw "Index out of range";
			}
		} catch(e) {
			console.log(e);
		}
	},

	setSrcVel: function(idx, velocity) {
		try {
			if (idx < this.sources.length) {
				this.sources[idx].velocity.x = velocity.x;
				this.sources[idx].velocity.y = velocity.y;
			} else {
				throw "Index out of range";
			}
		} catch(e) {
			console.log(e);
		}
	},

	setSrcTemp: function(idx, temperature) {
		try {
			if (idx < this.sources.length) {
				this.sources[idx].temperature = temperature;
			} else {
				throw "Index out of range";
			}
		} catch(e) {
			console.log(e);
		}
	},

	setSrcRadius: function(idx, radius) {
		try {
			if (idx < this.sources.length) {
				this.sources[idx].radius = radius;
			} else {
				throw "Index out of range";
			}
		} catch(e) {
			console.log(e);
		}
	}
};
