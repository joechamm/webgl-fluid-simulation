var Slab = function() {
	this.ping_surface = new Surface();
	this.pong_surface = new Surface();
};

Slab.prototype = {
	
	constructor: Slab,
	
	createFramebuffers: function() {
		this.ping_surface.createFramebuffer();
		this.pong_surface.createFramebuffer();
	},
	
	createTextures: function (internal_format, format, type, min_filter, mag_filter, s_wrap, t_wrap) {
		this.ping_surface.createTexture(internal_format,format,type,min_filter,mag_filter,s_wrap,t_wrap);
		this.pong_surface.createTexture(internal_format,format,type,min_filter,mag_filter,s_wrap,t_wrap);
	},
	
	setupFramebuffers: function() {
		this.ping_surface.setupFramebuffer();
		this.pong_surface.setupFramebuffer();
	},
	
	attachPingFBO: function() {
		this.ping_surface.attachFBO();
	},

	attachPongTex: function(texUnit) {
		this.pong_surface.attachTex(texUnit);
	},
	
	swapSurfaces: function() {
		var tempSurface = this.ping_surface;
		this.ping_surface = this.pong_surface;
		this.pong_surface = tempSurface;
	},

	clearPingSurface: function(r,g,b,a) {
		this.ping_surface.clearSurface(r,g,b,a);
	},

	clearPongSurface: function(r,g,b,a) {
		this.pong_surface.clearSurface(r,g,b,a);
	},
	
	clearSurfaces: function(r,g,b,a) {
		this.ping_surface.clearSurface(r,g,b,a);
		this.pong_surface.clearSurface(r,g,b,a);
	},
	
	destroySlab: function() {
		this.ping_surface.destroySurface();
		this.pong_surface.destroySurface();
	}
}