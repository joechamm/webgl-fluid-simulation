/**
 * Created by JetBrains WebStorm.
 * User: Cunningham
 * Date: 3/14/12
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
function VelTempSource(px, py, vx, vy, temp, rad){
    this.posX = px;
    this.posY = py;
    this.velX = vx;
    this.velY = vy;
    this.temperature = temp;
    this.radius = rad;

    this.getPosX = function(){
        return this.posX;
    }

    this.getPosY = function(){
        return this.posY;
    }

    this.getVelX = function(){
        return this.velX;
    }

    this.getVelY = function(){
        return this.velY;
    }

    this.getTemp = function(){
        return this.temperature;
    }

    this.getRadius = function(){
        return this.radius;
    }

    this.setPosX = function(px){
        this.posX = px;
    }

    this.setPosY = function(py){
        this.posY = py;
    }

    this.setVelX = function(vx){
        this.velX = vx;
    }

    this.setVelY = function(vy){
        this.velY = vy;
    }

    this.setTemp = function(tmp){
        this.temperature = tmp;
    }

    this.setRadius = function(rad){
        this.radius = rad;
    }
}

function SourceList(maxSources){
    this.max_sources = maxSources;
    this.sources = [];

    this.pushSource = function(src){
        if(this.sources.length < this.max_sources){
            this.sources.push(src);
        } else {
            alert("Maximum number of sources has been set.");
        }
    }

    this.popSource = function(){
        if(this.sources.length > 0){
            return this.sources.pop();
        } else {
            alert("No sources yet!");
        }
    }

    this.getSrcPosX = function(idx){
        if(idx < this.sources.length){
            return this.sources[idx].getPosX();
        } else {
            alert("Index out of range!");
        }
    }

    this.getSrcPosY = function(idx){
        if(idx < this.sources.length){
            return this.sources[idx].getPosY();
        } else {
            alert("Index out of range!");
        }
    }

    this.getSrcVelX = function(idx){
        if(idx < this.sources.length){
            return this.sources[idx].getVelX();
        } else {
            alert("Index out of range!");
        }
    }

    this.getSrcVelY = function(idx){
        if(idx < this.sources.length){
            return this.sources[idx].getVelY();
        } else {
            alert("Index out of range!");
        }
    }

    this.getSrcTemp = function(idx){
        if(idx < this.sources.length){
            return this.sources[idx].getTemp();
        } else {
            alert("Index out of range!");
        }
    }

    this.getSrcRadius = function(idx){
        if(idx < this.sources.length){
            return this.sources[idx].getRadius();
        } else {
            alert("Index out of range!");
        }
    }

    this.setSrcPosX = function(idx, val){
        if(idx < this.sources.length){
            this.sources[idx].setPosX(val);
        } else {
            alert("Index out of range!");
        }
    }

    this.setSrcPosY = function(idx, val){
        if(idx < this.sources.length){
            this.sources[idx].setPosY(val);
        } else {
            alert("Index out of range!");
        }
    }

    this.setSrcVelX = function(idx, val){
        if(idx < this.sources.length){
            this.sources[idx].setVelX(val);
        } else {
            alert("Index out of range!");
        }
    }

    this.setSrcVelY = function(idx, val){
        if(idx < this.sources.length){
            this.sources[idx].setVelY(val);
        } else {
            alert("Index out of range!");
        }
    }

    this.setSrcTemp = function(idx, val){
        if(idx < this.sources.length){
            this.sources[idx].setTemp(val);
        } else {
            alert("Index out of range!");
        }
    }

    this.setSrcRadius = function(idx, val){
        if(idx < this.sources.length){
            this.sources[idx].setRadius(val);
        } else {
            alert("Index out of range!");
        }
    }

    this.getNumSources = function(){
        return this.sources.length;
    }
}