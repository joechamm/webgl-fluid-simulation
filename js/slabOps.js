function Surface(width, height){
    this.width = width;
    this.height = height;
    this.fbo = null;
    this.tex = null;

    this.initSurface = function(gl){
        this.fbo = gl.createFramebuffer();
        this.tex = gl.createTexture();

        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.tex);

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.width, this.height, 0, gl.RGBA, gl.FLOAT, null);

        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.tex, 0);

        gl.bindTexture(gl.TEXTURE_2D, null);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }

    this.attachFBO = function(gl){
        gl.viewport(0, 0, this.width, this.height);
        if(!(gl.isFramebuffer(this.fbo))){
            alert("Surface does not have a valid framebuffer");
        } else {
            gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
        }
    }

    this.attachTex = function(gl, texUnit){
        if(!(gl.isTexture(this.tex))){
            alert("Surface does not have a valid texture");
        } else {
            gl.activeTexture(gl.TEXTURE0 + texUnit);
            gl.bindTexture(gl.TEXTURE_2D, this.tex);
        }
    }

    this.destroySurface = function(gl){
        if(gl.isFramebuffer(this.fbo)){
            gl.deleteFramebuffer(this.fbo);
        }
        if(gl.isTexture(this.tex)){
            gl.deleteTexture(this.tex);
        }
    }

    this.clearSurface = function(gl, rVal, gVal, bVal, aVal){
        gl.viewport(0, 0, this.width, this.height);
        if(!(gl.isFramebuffer(this.fbo))){
            alert("Surface does not have a valid framebuffer");
            return;
        } else {
            gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
        }
        gl.clearColor(rVal, gVal, bVal, aVal);
        gl.clear(this.gl.COLOR_BUFFER_BIT);
    }
}

function Slab(width, height){
    this.pingSurface = new Surface(width, height);  // write-to surface
    this.pongSurface = new Surface(width, height);  // read-from surface

    this.initSlab = function(gl){
        this.pingSurface.initSurface(gl);
        this.pongSurface.initSurface(gl);
    }

    this.swapSurfaces = function(){
        var tempSurface = this.pingSurface;
        this.pingSurface = this.pongSurface;
        this.pongSurface = tempSurface;
    }

    this.clearSurface = function(gl, surfId, rVal, gVal, bVal, aVal){
        if(surfId == 0){
            this.pingSurface.clearSurface(gl, rVal, gVal, bVal, aVal);
        } else if(surfId == 1){
            this.pongSurface.clearSurface(gl, rVal, gVal, bVal, aVal);
        } else {
            alert("Not a valid surface ID");
        }
    }

    this.attachPingFBO = function(gl){
        this.pingSurface.attachFBO(gl);
    }

    this.attachPongTex = function(gl, texUnit){
        this.pongSurface.attachTex(gl, texUnit);
    }

    this.destroySlab = function(gl){
        this.pingSurface.destroySurface(gl);
        this.pongSurface.destroySurface(gl);
    }
}