

function npot(val) {
	var v = val - 1; 
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4; 
	v |= v >> 8;
	v |= v >> 16;
	return v + 1;
}

var Surface = function() {
	this.fbo = {};
	this.tex = {};
};

Surface.prototype = {

	constructor: Surface,	
		
	createFramebuffer: function() {
		try {
			if (gl) {
				if (this.fbo instanceof WebGLFramebuffer) {
					throw new FramebufferException("Framebuffer object already exists!");
				} else {
					this.fbo = gl.createFramebuffer();
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	},
	
	createTexture: function (internal_format, format, type, min_filter, mag_filter, s_wrap, t_wrap) {
		try {
			if (gl) {
				if (this.tex instanceof WebGLTexture) {
					throw new TextureException("Texture object already exists!");
				} else {
					var width = npot(gl.drawingBufferWidth);
					var height = npot(gl.drawingBufferHeight);
				
					internal_format = internal_format !== undefined ? internal_format : gl.RGBA; // ALPHA, LUMINANCE, LUMINANCE_ALPHA, RGB[A]
					format = format !== undefined ? format : gl.RGBA; // ALPHA, RGB, RGBA LUMINANCE, LUMINANCE_ALPHA, (extension formats ...)
					type = type !== undefined ? type : gl.UNSIGNED_BYTE; // UNSIGNED_BYTE, UNSIGNED_SHORT_5_6_5, UNSIGED_SHORT_4_4_4_4, UNSIGNED_SHORT_5_5_5_1, (ext ftts ...)
					min_filter = min_filter !== undefined ? min_filter : gl.NEAREST; // NEAREST, LINEAR, NEAREST_MIPMAP_NEAREST, NEAREST_MIPMAP_LINEAR, LINEAR_MIPMAP_NEAREST, LINEAR_MIPMAP_LINEAR
					mag_filter = mag_filter !== undefined ? mag_filter : gl.NEAREST; // NEAREST, LINEAR
					s_wrap = s_wrap !== undefined ? s_wrap : gl.CLAMP_TO_EDGE; // CLAMP_TO_EDGE, REPEAT, MIRRORED_REPEAT
					t_wrap = t_wrap !== undefined ? t_wrap : gl.CLAMP_TO_EDGE;
					
					this.tex = gl.createTexture();
					gl.activeTexture(gl.TEXTURE0);
					gl.bindTexture(gl.TEXTURE_2D, this.tex);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, min_filter);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, mag_filter);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, s_wrap);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, t_wrap);
					gl.texImage2D(gl.TEXTURE_2D, 0, internal_format, width, height, 0, format, type, null);
					gl.bindTexture(gl.TEXTURE_2D, null);
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	},
	
	setupFramebuffer: function() {
		try {
			if (gl) {
				if (this.fbo instanceof WebGLFramebuffer && this.tex instanceof WebGLTexture) {
					var status;
					gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
					gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.tex, 0);
					status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
			
					if (status !== gl.FRAMEBUFFER_COMPLETE) {
						throw new FramebufferException(status);
					}
					
					gl.bindFramebuffer(gl.FRAMEBUFFER, null);
				} else {
					throw new FramebufferException("Framebuffer and Texture not yet created!");
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	},

	attachFBO: function() {
		try {
			if (gl) {
				if (this.fbo instanceof WebGLFramebuffer) {
					gl.viewport(0,0,gl.drawingBufferWidth,gl.drawingBufferHeight);
					gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
				} else {
					throw new FramebufferException("Invalid Framebuffer object");
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	},

	attachTex: function(unit) {
		try {
			if (gl) {
				if (this.tex instanceof WebGLTexture) {
					gl.activeTexture(gl.TEXTURE0 + unit);
					gl.bindTexture(gl.TEXTURE_2D, this.tex);
				} else {
					throw new TextureException("Invalid Texture object");
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	},

	destroySurface: function() {
		try {
			if (gl) {
				gl.deleteFramebuffer(this.fbo);
				gl.deleteTexture(this.tex);
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	},

	clearSurface: function(r, g, b, a) {
		try {
			if (gl) {
				if (this.fbo instanceof WebGLFramebuffer) {
					gl.viewport(0,0,gl.drawingBufferWidth,gl.drawingBufferHeight);
					gl.clearColor(r,g,b,a);
					gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
					gl.clear(gl.COLOR_BUFFER_BIT);
					gl.bindFramebuffer(gl.FRAMEBUFFER, null);
				} else {
					throw new FramebufferException("Invalid Framebuffer object");
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	}
};
