
var FluidSource = function(position, velocity, temperature, radius) {
	this.position = position;
	this.velocity = velocity;
	this.temperature = temperature;
	this.radius = radius;
};

FluidSource.prototype = {

	constructor: FluidSource,
	
	toString: function() {
		var str = "[FluidSource Position: " + this.position;
		str += " Velocity: " + this.velocity;
		str += " Temperature: " + this.temperature;
		str += " Radius: " + this.radius + "]";
		return str;
	}
};
