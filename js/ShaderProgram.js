

var ShaderProgram = function () {
	this.program = {};
	this.attributes = {};
	this.uniforms = {};
};

ShaderProgram.prototype = {
	
	constructor: ShaderProgram,
		
	createVertexShader: function (shader_src) {
		try {
			if (gl) {
				var vs = gl.createShader(gl.VERTEX_SHADER);
				gl.shaderSource(vs,shader_src);
				gl.compileShader(vs);
		
				if (!gl.getShaderParameter(vs, gl.COMPILE_STATUS)) {
					throw new ShaderException(gl.getShaderInfoLog(vs));
				}
				return vs;
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
			return null;
		}					
	},
	
	createFragmentShader: function (shader_src) {
		try {
			if (gl) {
				var fs = gl.createShader(gl.FRAGMENT_SHADER);
				gl.shaderSource(fs,shader_src);
				gl.compileShader(fs);
		
				if (!gl.getShaderParameter(fs, gl.COMPILE_STATUS)) {
					throw new ShaderException(gl.getShaderInfoLog(fs));
				}
				return fs;
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
			return null;
		}					
	},

	initProgram: function (vs,fs) {
	
		try {
			if (gl) {
				if (this.program instanceof WebGLProgram) {
					throw new ProgramException("Program already exists!");
				} else {
					this.program = gl.createProgram();
					gl.attachShader(this.program, vs);
					gl.attachShader(this.program, fs);
					
					if (this.attributes !== undefined) {
						var that = this,
							akeys = Object.keys(this.attributes);
							akeys.forEach(function(key) {
								var val = that.attributes[key];
								gl.bindAttribLocation(that.program, val, key);
							}
						);
					}

					gl.linkProgram(this.program);
					
					if (!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
						throw new ProgramException("Program failed to link.");
					}
					
					this.attributes = {};
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
			if (e.name == "ProgramException") {
				gl.deleteProgram(this.program);
				this.program = {};
			}
		}
	},
	
	destroyProgram: function() {
		
		try {
			if (gl) {
				if (this.program instanceof WebGLProgram) {
					gl.deleteProgram(this.program);
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
		
		this.program = {};
	},

	isProgram: function() {
		try {
			if (gl) {
				return gl.isProgram(this.program);
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
			return false;
		}
	},

	useProgram: function() {
		try {
			if (gl) {
				gl.useProgram(this.program);
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	},

	setupUniforms: function() {
		
		try {
			if (gl) {
				var uniform_count = gl.getProgramParameter(this.program, gl.ACTIVE_UNIFORMS);
				if (uniform_count !== null && uniform_count !== gl.INVALID_ENUM) {
					this.uniforms = {};
					for (var i = 0; i < uniform_count; i++) {
						var uniform_info = gl.getActiveUniform(this.program, i),
							uniform_location = gl.getUniformLocation(this.program, uniform_info.name);
				
						this.uniforms[uniform_info.name] = {
							type: uniform_info.type,
							location: uniform_location
						};							
					}
				} else {
					throw new ProgramException("Invalid Program object");
				}
			} else {
				throw new ContextException("No WebGL Context!");
			}
		} catch(e) {
			console.log(e);
		}
	}
};
		
					

